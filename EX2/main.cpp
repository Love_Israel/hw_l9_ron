#include <iostream>
#include <ostream>
#include "functions.h"

using std::ostream;



int main () 
{
	myClass c = myClass(12);
	myClass c1 = myClass(8);
	myClass arr_c[2] = { c , c1 };
	std::cout << compare(1.5, 3.5) << "\n";
	std::cout << compare('a', 'b') << "\n";
	std::cout << compare(c, c1) << "\n";
	int arr[5] = { 1 , 4 , 2 , 8 , 3 };
	int arr2[5] = { 'a' , 'b' , 'g' , 's' , 'd' };
	bubbleSort(arr, 5);
	bubbleSort(arr2, 5);
	printArr(arr, 5);
	printArr(arr2, 5);
	printArr(arr_c, 2);
}