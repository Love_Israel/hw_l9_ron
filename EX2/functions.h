#pragma once
#include <iostream>
using std::ostream;

class myClass
{
public:
	int n;
	myClass(int num)
	{
		n = num;
	}
	myClass& operator= (myClass& other)
	{
		myClass* mc = new myClass(n);
		return *mc;
	}
	bool operator> (myClass& other)
	{
		return (n > other.n);
	}
	bool operator< (myClass& other)
	{
		return (n < other.n);
	}
	bool operator== (myClass& other) {
		return (n == other.n);
	}
	friend ostream& operator<< (ostream& out, const myClass& c);

	
};

ostream& operator<< (ostream& out, const myClass& c)
{
	out << c.n;
	return out;
}

template<typename T>
int compare (T obj1 , T obj2) 
{
	if (obj1 == obj2)
		return 0;
	return (obj1 > obj2) ? 1 : -1;
}

template<typename T>
void swap(T* xp, T* yp)
{
	T temp = *xp;
	*xp = *yp;
	*yp = temp;
}

template <typename T> 
void bubbleSort(T arr[], int n)
{
	int i, j;
	bool swapped;
	for (i = 0; i < n - 1; i++)
	{
		swapped = false;
		for (j = 0; j < n - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(&arr[j], &arr[j + 1]);
				swapped = true;
			}
		}
 
		if (swapped == false)
			break;
	}
}

template <typename T>
void printArr (T arr , int n) 
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i];
	std::cout << "\n";
}

