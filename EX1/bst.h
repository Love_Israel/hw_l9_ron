#pragma once
#include <iostream>

using std::string;

template<typename T>
class BSNode 
{
	BSNode<T>* _right;
	BSNode<T>* _left;
	T _data;
	int _count;

public:
	BSNode() {};
	BSNode (T data) 
	{
		_data = data;
		_right = nullptr;
		_left = nullptr;
		_count = 1;
	}
	~BSNode () 
	{
		deleteNodes(this);
	}

	void deleteNodes (BSNode* n) 
	{
		if (!n)
			return;
		BSNode* l = n->getLeft();
		BSNode* r = n->getRight();
		delete (n);
		deleteNode(l);
		deleteNode(r);
	}
	/*BSNode (const BSNode& other) 
	{
		BSNode n = other;
		_data = n.getData();
		_right = n.getRight();
		_left = n.getLeft();

	}*/

	int getApper () 
	{
		return _count;
	}

	void AppCount () 
	{
		_count++;
	}
	void setRight (BSNode* r) 
	{
		_right = r;
	}
	void setLeft (BSNode* l) 
	{
		_left = l;
	}

	T getData () const
	{
		return _data;
	}

	BSNode* getRight () const
	{
		return _right;
	}

	BSNode* getLeft () const
	{
		return _left;
	}

	void setData(T data) 
	{
		_data = data;
	}
	BSNode* insertHelper (BSNode* root , T data) 
	{
		if (root == nullptr)
			return new BSNode(data);
		if (root->getData() == data) 
			root->AppCount();
		else if (data > root->getData())
			root->setRight(insertHelper(root->getRight(), data));
		else
			root->setLeft(insertHelper(root->getLeft(), data));
		return root;
	}

	void insert (T data)
	{
		insertHelper(this, data);
	}

	bool searchHelper (BSNode* root , T data) 
	{
		if (root == nullptr)
			return false;
		if (root->getData() == data)
			return true;
		if (data > root->getData())
			return searchHelper(root->getRight(), data);
		else
			return searchHelper(root->getLeft(), data);
	}

	bool search (T data) 
	{
		return searchHelper(this, data);
	}

	int getHeightHelper (BSNode* root) 
	{
		if (root == nullptr)
			return 0;
		int rNode = getHeightHelper(root->getRight());
		int lNode = getHeightHelper(root->getRight());

		return (rNode > lNode) ? (rNode + 1) : (lNode + 1);
	}

	int getHeight () 
	{
		return getHeightHelper(this);
	}

	bool isLeaf() 
	{
		return (!_right && !_left) ? true : false;
	}

	int getDepthHelper (BSNode * root , BSNode * node) 
	{
		int currDepth;
		if (!root)
		if (root->getData() == node->getData())
			return 0;
		if (root->getData() > node->getData())
			currDepth = getDepthHelper(root->getRight(), node);
		else
			currDepth = getDepthHelper(root->getLeft(), node);
		return currDepth + 1;
	}

	int getDepth (BSNode& root) 
	{
		//if (!(root.search(this->_data)))
			//return -1;
		return getDepthHelper(&root, this);
	}

	void deepCopy (BSNode * node , const BSNode * other) 
	{
		BSNode* l = nullptr; BSNode* r = nullptr;
		if (node) {
			l = node->getLeft();
			r = node->getRight();
		}
		if (!other)
			node = nullptr;
		else 
		{
			node = new BSNode(other->getData());
			deepCopy(l ? l : node->getLeft(), other->getLeft());
			deepCopy(r ? r : node->getRight() , other->getRight());
		}
	}

	BSNode& operator= (const BSNode& other) 
	{
		BSNode* nNode = new BSNode();
		deepCopy(nNode, &other);
		return *nNode;
	}
	void printWordsHelper (BSNode* n) 
	{
		if (n) 
		{
			printWordsHelper(n->getLeft());
			std::cout << n->getData() << " " << n->getApper() << "\n";
			printWordsHelper(n->getRight());
		}
	}
	void printWords () 
	{
		printWordsHelper(this);
	}

	BSNode* minValueNode(BSNode* node)
	{
		BSNode* current = node;

		/* loop down to find the leftmost leaf */
		while (current && current->getLeft() != NULL)
			current = current->getLeft();

		return current;
	}

	BSNode* deleteN (T data) 
	{
		return deleteNode(this, data);
	}

	BSNode* deleteNode(BSNode* root, T key)
	{
		if (root == NULL) return root;


		if (key < root->getData())
			root->setLeft(deleteNode(root->getLeft(), key));


		else if (key > root->getData())
			root->setRight(deleteNode(root->getRight(), key));

		else
		{
			if (root->getLeft() == NULL)
			{
				BSNode* temp = root->getRight();
				free(root);
				return temp;
			}
			else if (root->getRight() == NULL)
			{
				BSNode* temp = root->getLeft();
				free(root);
				return temp;
			}


			BSNode* temp = minValueNode(root->getRight());
			root->setData(temp->getData());
			root->setRight(deleteNode(root->getRight(), temp->getData()));
		}
		return root;
	}

};